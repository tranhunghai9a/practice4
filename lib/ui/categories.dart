import 'package:flutter/material.dart';
import './home.dart';

class CategoriesScreen extends StatefulWidget {
  static const route= '/categories';
  @override
  State<StatefulWidget> createState() {
    return CategoriesScreenState();
  }

}

class CategoriesScreenState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Categories Screen'),),
        body: Column(
          children: [
            Expanded(child: _buildSearch(context)),
            Expanded(child: _buildList(context))
          ],
        )
    );
  }

  Widget _buildSearch(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildControls()
        ],
      ),
    );
  }

  Row _buildControls() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(HomeScreen.route);

            },
            child: Icon(Icons.home)
        ),
        SizedBox(width: 20),
        Text('Thanh Tra cứu ở đây', style: Theme.of(context)
            .textTheme
            .headline5!
            .copyWith(color: Colors.white),
        ),
        SizedBox(height: 20),
      ],
    );
  }

  Widget _buildList(BuildContext context) {
    return Scrollbar(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20),
          _buildListControl()
        ],
      ),
    );
  }

  Row _buildListControl() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('sản phẩm ở đây', style: Theme.of(context)
            .textTheme
            .headline3!
            .copyWith(color: Colors.black),
        ),
        SizedBox(height: 20)
      ],
    );
  }



}