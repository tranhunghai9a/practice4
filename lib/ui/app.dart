import 'package:flutter/material.dart';
import './home.dart';
import './categories.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => HomeScreen(),
        '/home': (context) => HomeScreen(),
        '/categories': (context) => CategoriesScreen()
      },
      initialRoute: '/',
    );
  }

}
