import 'package:flutter/material.dart';
import './categories.dart';

class HomeScreen extends StatefulWidget {
  static const route= '/home';
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }

}

class HomeScreenState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home Screen'),),
      body: Column(
        children: [
          Expanded(child: _buildSearch(context)),
          Expanded(child: _buildList(context)),
          Expanded(child: _buildFooter(context))
        ],
      )
    );
  }

  Widget _buildSearch(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Thanh Tra cứu ở đây', style: Theme.of(context)
              .textTheme
              .headline5!
              .copyWith(color: Colors.white),
          ),
          SizedBox(height: 20),
          _buildControls()
        ],
      ),
    );
  }

  Row _buildControls() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(CategoriesScreen.route);

            },
            child: Text('CATEGORIES')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('những')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('phím')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('Screen')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('ở')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('đây')
        ),
      ],
    );
  }

  Widget _buildList(BuildContext context) {
    return Scrollbar(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20),
          _buildListControl()
        ],
      ),
    );
  }

  Row _buildListControl() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('sản phẩm ở đây', style: Theme.of(context)
            .textTheme
            .headline3!
            .copyWith(color: Colors.black),
        ),
        SizedBox(height: 20)
      ],
    );
  }


  Widget _buildFooter(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 20),
          _buildFooterControl()
        ],
      ),
    );
  }

  Row _buildFooterControl() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {
              Navigator.of(context).pushReplacementNamed(CategoriesScreen.route);

            },
            child: Text('FEED')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('MESSAGE')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('CART')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('ACCOUNT')
        ),
        SizedBox(width: 20)
      ],
    );
  }

}